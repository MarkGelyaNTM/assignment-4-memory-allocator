#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "test.h"

// Функция для вывода сообщения в консоль на основе результата теста
void printTestResult(struct testResult result) {
    if (result.status)
        printf("PASSED: %s\n", result.msg);
    else
        printf("FAILED: %s\n", result.msg);
}

int main() {
    printTestResult(test_simple_allocate());
    printTestResult(test_free_block());
    printTestResult(test_free_blocks());
    printTestResult(test_expand_region());
    printTestResult(test_allocate_new_region());
    printTestResult((struct testResult) { .status=true, .msg="FINISH"});
    return 0;
}
