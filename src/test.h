#pragma once

#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

#define SIZE 100

struct testResult {
    bool status;
    const char *msg;
};

struct testResult test_simple_allocate();
struct testResult test_free_block();
struct testResult test_free_blocks();
struct testResult test_expand_region();
struct testResult test_allocate_new_region();
