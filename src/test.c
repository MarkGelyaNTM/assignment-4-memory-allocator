#include "test.h"

static struct block_header* get_header(void* content) {
    return (struct block_header*) (content - offsetof(struct block_header, contents));
}

struct testResult test_simple_allocate() {
    void* heap = heap_init(0);
    void* block = _malloc(SIZE);

    if (heap == NULL)
        return (struct testResult) {
            .status = false,
            .msg = "heap_init(0) returned NULL",
        };

    if (block == NULL)
        return (struct testResult) {
            .status = false,
            .msg = "_malloc(SIZE) returned NULL",
        };

    _free(block);
    heap_term();

    return (struct testResult) {
        .status = true,
        .msg = "test_simple_allocate",
    };
}

struct testResult test_free_block() {
    heap_init(0);
    void* mem1 = _malloc(SIZE);
    void* mem2 = _malloc(SIZE);
    void* mem3 = _malloc(SIZE);
    struct block_header* header1 = get_header(mem1);
    struct block_header* header2 = get_header(mem2);
    struct block_header* header3 = get_header(mem3);
    if (header1->is_free || header2->is_free || header3->is_free) {
        return (struct testResult) {
            .status = false,
            .msg = "_malloc error",
        };
    }
    _free(mem2);
    if (header1->is_free || !header2->is_free || header3->is_free) {
        return (struct testResult) {
            .status = false,
            .msg = "_free error",
        };
    }

    heap_term();

    return (struct testResult) {
        .status = true,
        .msg = "test_free_block",
    };
}

struct testResult test_free_blocks() {
    heap_init(0);
    void* mem1 = _malloc(SIZE);
    void* mem2 = _malloc(SIZE);
    void* mem3 = _malloc(SIZE);
    struct block_header* header1 = get_header(mem1);
    struct block_header* header2 = get_header(mem2);
    struct block_header* header3 = get_header(mem3);
    if (header1->is_free || header2->is_free || header3->is_free) {
        return (struct testResult) {
            .status = false,
            .msg = "_malloc error",
        };
    }
    _free(mem2);
    _free(mem3);
    if (header1->is_free || !header2->is_free || !header3->is_free) {
        return (struct testResult) {
            .status = false,
            .msg = "_free error",
        };
    }

    heap_term();

    return (struct testResult) {
        .status = true,
        .msg = "test_free_blocks",
    };
}


struct testResult test_expand_region() {
    struct region* region = heap_init(0);
    size_t old_size = region->size;
    _malloc(region->size * 2);
    size_t new_size = region->size;
    heap_term();
    if (old_size > new_size)
        return (struct testResult){
            .status = false,
            .msg = "test_expand_region",
        };
    return (struct testResult){
        .status = true,
        .msg = "test_expand_region",
    };
}

/* mem.c */
void* map_pages(void const* addr, size_t length, int additional_flags);

struct testResult test_allocate_new_region() {
    heap_init(SIZE);
    void* ptr1 = map_pages(HEAP_START + SIZE, REGION_MIN_SIZE, MAP_FIXED);
    void* ptr2 = _malloc(SIZE + 1);
    heap_term();
    if (ptr1 == ptr2)
        return (struct testResult) {
            .status = false,
            .msg = "test_allocate_new_region",
        };
    return (struct testResult){
        .status = true,
        .msg = "test_allocate_new_region",
    };
}
